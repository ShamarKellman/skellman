<div class="container mx-auto px-2 py-10 w-full flex flex-col" id="contact">
  <h2 class="uppercase text-center text-3xl text-blue-500 font-bold">Contact Me</h2>
  <h4 class="uppercase text-center text-xl text-blue-300 font-bold mb-6">Please feel free to contact me!</h4>
  <div class="w-full flex flex-col-reverse items-center lg:flex-row">
    <div class="w-full lg:w-1/2 p-2 text-blue-500">
      {!! do_shortcode( "[ninja_form id='2']") !!}
    </div>
    <div class="w-full lg:w-1/2 p-2">
      <img src="@asset('images/contact.svg')" alt="">
    </div>
  </div>
</div>
