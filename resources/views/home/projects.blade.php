<div class="w-full bg-blue-500 flex flex-col py-10" id="projects">
  <h2 class="uppercase text-center text-3xl text-white font-bold  mb-6">Projects</h2>
  <div class="container mx-auto px-2 flex flex-col md:flex-row justify-around">
    @if($projects->have_posts())
      @foreach($projects->posts as $project)
        @include('home.partials.project-card', ['project' => $project])
      @endforeach
    @else
      <div class="w-full flex flex-col h-auto items-center justify-center my-16">
        <p class="text-white uppercase font-bold text-4xl ">Working on those PROJECTS</p>
      </div>
    @endif
  </div>
</div>
