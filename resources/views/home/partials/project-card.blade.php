@php
  $fields = (object)get_fields($project->ID);

  $statuses = [
        'planning' => 'In Planning',
        'progress' => 'In Progress',
        'completed' => 'Completed',
    ];
@endphp
<div class="flex flex-col w-full md:w-1/3 rounded-lg bg-white p-4 mx-0 md:mx-2 my-2" style="height:  24rem;">
  <h3 class="font-bold text-2xl text-blue-500 text-center">{{ $project->post_title }}</h3>
  <p class="opacity-75 text-blue-300 text-center">{{ $statuses[$fields->project_status] }}</p>
  <div class=" flex flex-1 flex-wrap justify-center items-center">
    @foreach($fields->project_tech as $tech)
      <img src="{{ \App\asset_path('images/langs/' . $tech . '.svg') }}" class="w-20 h-20 mx-2" alt="">
      @break($loop->iteration == 4)
    @endforeach
  </div>
  @isset($fields->project_url)
  <div class="w-full">
    <a href="{{ $fields->project_url }}" class="bg-blue-500 text-white w-full py-2 block rounded-lg font-bold hover:bg-blue-300 text-center">VIEW</a>
  </div>
  @endisset
</div>
