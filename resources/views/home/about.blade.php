<div class="container mx-auto px-2 py-10 w-full flex flex-col" id="about">
  <h2 class="uppercase text-center text-3xl text-blue-500 font-bold">About Me</h2>
  <div class="w-full flex flex-col lg:flex-row">
    <div class="w-full lg:w-1/2 p-2">
      <img src="@asset('images/about.svg')" alt="">
    </div>
    <div class="w-full lg:w-1/2 p-2 text-blue-500">
      <h3 class="text-2xl font-bold text-center">Shamar Kellman</h3>
      <h5 class="text-lg text-center text-blue-400 opacity-75">{{ $job_title }}</h5>
      <p class="text-lg text-center text-gray-500">Barbados</p>

      <p class="text-justify mt-8">{{ $about_text }}</p>

      <div class="mt-8 flex justify-center">
        <a href="#contact" class="bg-blue-500 text-white px-6 py-3 rounded-lg text-lg focus:outline-none block hover:bg-blue-300">Contact Me</a>
      </div>

      <div class="mt-12 w-full flex justify-center flex-wrap text-5xl">
        <a href="{{ $linkedin }}" class="hover:no-underline"><i class="fab fa-linkedin-in mx-4"></i></a>
        <a href="{{ $github }}" class="hover:no-underline"><i class="fab fa-github mx-4"></i></a>
        <a href="{{ $stackoverflow }}" class="hover:no-underline"><i class="fab fa-stack-overflow mx-4"></i></a>
        <a href="{{ $px }}" class="hover:no-underline"><i class="fab fa-500px mx-4"></i></a>
      </div>
    </div>
  </div>
</div>
