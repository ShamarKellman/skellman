@extends('layouts.app')
@php
  setPostViews(get_the_ID());
@endphp

@section('content')
  <div class="flex flex-col md:flex-row max-w-5xl mb-6 mx-auto px-2 md:px-0">
    <div class="w-full flex flex-col" id="content">
      @if(has_post_thumbnail( $post->ID ))
        {!! get_the_post_thumbnail($post->ID, 'large') !!}
      @else
        <img src="https://placehold.it/1024x500" alt="featured_image">
      @endif
      <div class="mt-4">
        <h1 class="text-center text-3xl md:text-5xl text-blue-500 font-bold">{!! $post->post_title !!}</h1>
        <div class="text-lg leading-relaxed mb-4 border-b-2 border-gray-200 prose max-w-none">
          {!! $post->post_content !!}
        </div>
      </div>
      <div class="flex flex-col md:flex-row md:justify-between">
        <div class="inline-flex items-center border-none">
          <img alt="blog" src="{{ get_avatar_url($post->post_author) }}"
               class="w-12 h-12 rounded-full flex-shrink-0 object-cover object-center">
          <span class="flex-grow flex flex-col pl-4">
            <span class="title-font font-medium text-gray-900">
              {{ get_the_author_meta('first_name', $post->post_author) . " " . get_the_author_meta('last_name', $post->post_author)  }}
            </span>
            <span class="text-gray-500 text-sm uppercase">Web Applications DEVELOPER</span>
          </span>
        </div>
        <div class="my-4 md:mt-0 flex-col justify-center">
          <p class="text-center md:text-left">Share To:</p>
          <div class="flex text-3xl justify-center md:justify-start">
            <a href="https://www.facebook.com/sharer/sharer.php?t={{ $share_title }}&amp;u={{ $share_url }}" class="mx-2" target="_blank"><i class="fab fa-facebook-f"></i></a>
            <a href="https://twitter.com/intent/tweet?text={{ $share_title }}&amp;url={{ $share_url }}&amp;via=ShamarTheCoder" class="mx-2" target="_blank"><i class="fab fa-twitter"></i></a>
            <a href="https://api.whatsapp.com/send?phone=&amp;text={{ $whatsapp_text}}" class="mx-2" target="_blank"><i class="fab fa-whatsapp"></i></a>
            <a href="https://www.linkedin.com/sharing/share-offsite/?url={{ $share_url }}" class="mx-2" target="_blank"><i class="fab fa-linkedin"></i></a>
          </div>
        </div>
      </div>
        @php comments_template('/partials/comments.blade.php') @endphp
    </div>
  </div>
@endsection
