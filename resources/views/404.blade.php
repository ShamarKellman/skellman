@extends('layouts.app')

@section('content')
  <div class="flex flex-col justify-center items-center my-auto py-10">
    <div class="flex flex-col max-w-lg mx-4">
      <img src="@asset('images/404.svg')" alt="404">
      <h1 class="text-5xl text-center">Oops!!! Looks like an empty box.</h1>

      <a href="/" class="text-center hover:no-underline border-none hover:text-blue-400 mt-4">Return to where you can find information.</a>
    </div>
  </div>
@endsection
