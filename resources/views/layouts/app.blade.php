<!doctype html>
<html {!! get_language_attributes() !!} style="scroll-behavior: smooth;">
  @include('partials.head')
  <body @php body_class('flex flex-col h-screen antialiased font-sans') @endphp >
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap flex flex-1" role="document">
      <div class="content w-full">
        <main class="main w-full">
          @yield('content')
        </main>
{{--        @if (App\display_sidebar())--}}
{{--          <aside class="sidebar">--}}
{{--            @include('partials.sidebar')--}}
{{--          </aside>--}}
{{--        @endif--}}
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
