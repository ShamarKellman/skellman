@php/*
Template Name: Full-width layout
Template Post Type: post
*/
@endphp

@extends('layouts.app')

@section('content')
  <div class="container px-5 my-6 mx-auto">
    <h1 class="text-center text-blue-500 text-3xl md:text-5xl mb-4">Blog</h1>
    <div class="flex flex-wrap">
      @if($posts->have_posts())
       @foreach($posts->posts as $post)
        <div class="md:p-12 md:w-1/2 flex flex-col items-start mb-6 md:mb-0">
          <a href="{{ get_category_link(get_the_category($post->ID)[0]->term_id) }}" class="inline-block py-1 px-3 rounded bg-indigo-100 text-indigo-500 text-sm font-medium tracking-widest uppercase">{{ get_the_category($post->ID)[0]->name }}</a>
          <a href="{{ get_permalink($post->ID) }}" class="hover:no-underline border-none">
            <h2 class="sm:text-3xl text-2xl title-font font-medium mt-4 mb-4">{{ get_the_title($post->ID) }}</h2>
          </a>
          <p class="leading-relaxed mb-8 text-justify md:text-left">{!! get_the_excerpt($post->ID) !!}</p>
          <div class="flex items-center flex-wrap pb-4 mb-4 border-b-2 border-gray-200 mt-auto w-full">
            <a href="{{ get_permalink($post->ID) }}" class="text-blue-500 hover:text-blue-700 inline-flex items-center">Learn More
              <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" fill="none"
                   stroke-linecap="round" stroke-linejoin="round">
                <path d="M5 12h14"></path>
                <path d="M12 5l7 7-7 7"></path>
              </svg>
            </a>
            <span
              class="text-gray-600 mr-3 inline-flex items-center ml-auto leading-none text-sm pr-3 py-1 border-r-2 border-gray-300">
              <svg class="w-4 h-4 mr-1" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round"
                   stroke-linejoin="round" viewBox="0 0 24 24">
                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                <circle cx="12" cy="12" r="3"></circle>
              </svg>{{ getPostViews($post->ID) }}
            </span>
            <span class="text-gray-600 inline-flex items-center leading-none text-sm">
              <svg class="w-4 h-4 mr-1" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round"
                   stroke-linejoin="round" viewBox="0 0 24 24">
                <path
                  d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
              </svg>{{ get_comments_number($post->ID) }}
            </span>
          </div>
          <a class="inline-flex items-center border-none">
            <img alt="blog" src="{{ get_avatar_url($post->post_author) }}"
                 class="w-12 h-12 rounded-full flex-shrink-0 object-cover object-center">
            <span class="flex-grow flex flex-col pl-4">
              <span class="title-font font-medium text-gray-900">
                {{ get_the_author_meta('first_name', $post->post_author) . " " . get_the_author_meta('last_name', $post->post_author)  }}
              </span>
              <span class="text-gray-500 text-sm uppercase">Web Applications DEVELOPER</span>
            </span>
          </a>
        </div>
        @endforeach
      @endif
    </div>
  </div>
@endsection
