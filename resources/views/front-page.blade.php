@extends('layouts.app')

@section('content')
  <div class="w-full bg-blue-500 flex flex-col py-24" id="home">
    <div class="container mx-auto px-2 flex flex-col md:flex-row">
      <div class="flex flex-col justify-center w-full lg:w-1/2 text-white text-center md:text-left">
        <h1 class="uppercase font-bold text-3xl md:text-5xl">Developing Your Ideas</h1>
        <p class="text-lg md:text-2xl">Investing In Your Future</p>
        <div class="mt-10">
          <a href="#contact" class="py-6 px-16 bg-white text-2xl font-bold rounded-lg text-blue-500 hover:bg-blue-100 hover:text-white focus:outline-none">REACH OUT</a>
        </div>
      </div>
      <div class="flex justify-center mt-10 md:mt-0">
        <img class="h-56 max max-w-md md:h-auto" src="@asset('images/landing-image.svg')" alt="">
      </div>
    </div>
  </div>
  @include('home.about')
  @include('home.projects')
  @include('home.contact')
@endsection
