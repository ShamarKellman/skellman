<footer class="w-full bg-blue-500 text-white">
  <div class="container mx-auto px-2 py-16 text-center">
    &copy; Shamar Kellman {{ date('Y') }}
  </div>
</footer>
