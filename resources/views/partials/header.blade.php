<header class="bg-blue-500 text-white py-4 px-2">
  <div class="container mx-auto px-2 md:px-0 flex flex flex-col md:flex-row items-center md:justify-between">
    <a class="text-white text-xl no-underline hover:no-underline uppercase font-bold text-center md:text-left" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
    <nav class="nav-primary text-white">
      <a href="/#home" class="text-white hover:text-blue-100 ml-0 md:mr-4">Home</a>
      <a href="/blog" class="text-white hover:text-blue-100 mx-2 md:mx-4">Blog</a>
      <a href="/#projects" class="text-white hover:text-blue-100 mx-2 md:mx-4">Projects</a>
      <a href="/#contact" class="text-white hover:text-blue-100 mr-0 md:ml-4">Contact</a>
    </nav>
  </div>
</header>
