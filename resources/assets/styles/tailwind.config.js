module.exports = {
  purge: [
    './resources/views/**/*.html',
    './resources/views/**/*.php',
    './resources/views/**/*.js',
    './resources/assets/**/*.css',
    './resources/assets/**/*.scss',
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#018EC6',
        'blue': {
          default: '#018EC6',
          '100': '#94E0FE',
          '200': '#62D2FE',
          '300': '#2FC3FE',
          '400': '#01B3F9',
          '500': '#018EC6',
          '600': '#016A93',
          '700': '#004560',
          '800': '#00212E',
          '900': '#00FCFB'
        },
      }
    },
    fontFamily: {
      'sans': ['Balsamiq Sans', 'Helvetica, Arial, sans-serif']
    },
    typography: {
      default: {
        css: {
          h1: {
            color: '#018EC6',
          }
        },
      },
    }
  },
  variants: {
    borderStyle: ['responsive', 'focus'],
    borderWidth: ['responsive', 'focus'],
    borderRadius: ['responsive', 'focus'],
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
