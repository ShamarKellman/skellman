<?php


namespace App;


class SKellmanOption {
    private $skellman_option_options;

    public function __construct() {
        add_action( 'admin_menu', array( $this, 'skellman_option_add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'skellman_option_page_init' ) );
    }

    public function skellman_option_add_plugin_page() {
        add_menu_page(
            'SKellman Option', // page_title
            'SKellman Option', // menu_title
            'manage_options', // capability
            'skellman-option', // menu_slug
            array( $this, 'skellman_option_create_admin_page' ), // function
            'dashicons-admin-generic', // icon_url
            3 // position
        );
    }

    public function skellman_option_create_admin_page() {
        $this->skellman_option_options = get_option( 'skellman_option_option_name' ); ?>

        <div class="wrap">
            <h2>SKellman Option</h2>
            <p></p>
            <?php settings_errors(); ?>

            <form method="post" action="options.php">
                <?php
                settings_fields( 'skellman_option_option_group' );
                do_settings_sections( 'skellman-option-admin' );
                submit_button();
                ?>
            </form>
        </div>
    <?php }

    public function skellman_option_page_init() {
        register_setting(
            'skellman_option_option_group', // option_group
            'skellman_option_option_name', // option_name
            array( $this, 'skellman_option_sanitize' ) // sanitize_callback
        );

        add_settings_section(
            'skellman_option_setting_section', // id
            'Settings', // title
            array( $this, 'skellman_option_section_info' ), // callback
            'skellman-option-admin' // page
        );

        add_settings_field(
            'about_text_0', // id
            'About Text', // title
            array( $this, 'about_text_0_callback' ), // callback
            'skellman-option-admin', // page
            'skellman_option_setting_section' // section
        );

        add_settings_field(
            'about_job_title_1', // id
            'About Job Title', // title
            array( $this, 'about_job_title_1_callback' ), // callback
            'skellman-option-admin', // page
            'skellman_option_setting_section' // section
        );

        add_settings_field(
            'linkedin_2', // id
            'LinkedIn', // title
            array( $this, 'linkedin_2_callback' ), // callback
            'skellman-option-admin', // page
            'skellman_option_setting_section' // section
        );

        add_settings_field(
            'github_3', // id
            'Github', // title
            array( $this, 'github_3_callback' ), // callback
            'skellman-option-admin', // page
            'skellman_option_setting_section' // section
        );

        add_settings_field(
            'stackoverflow_4', // id
            'Stackoverflow', // title
            array( $this, 'stackoverflow_4_callback' ), // callback
            'skellman-option-admin', // page
            'skellman_option_setting_section' // section
        );

        add_settings_field(
            'px_5', // id
            '500px', // title
            array( $this, 'px_5_callback' ), // callback
            'skellman-option-admin', // page
            'skellman_option_setting_section' // section
        );
    }

    public function skellman_option_sanitize($input) {
        $sanitary_values = array();
        if ( isset( $input['about_text_0'] ) ) {
            $sanitary_values['about_text_0'] = esc_textarea( $input['about_text_0'] );
        }

        if ( isset( $input['about_job_title_1'] ) ) {
            $sanitary_values['about_job_title_1'] = sanitize_text_field( $input['about_job_title_1'] );
        }

        if ( isset( $input['linkedin_2'] ) ) {
            $sanitary_values['linkedin_2'] = sanitize_text_field( $input['linkedin_2'] );
        }

        if ( isset( $input['github_3'] ) ) {
            $sanitary_values['github_3'] = sanitize_text_field( $input['github_3'] );
        }

        if ( isset( $input['stackoverflow_4'] ) ) {
            $sanitary_values['stackoverflow_4'] = sanitize_text_field( $input['stackoverflow_4'] );
        }

        if ( isset( $input['px_5'] ) ) {
            $sanitary_values['px_5'] = sanitize_text_field( $input['px_5'] );
        }

        return $sanitary_values;
    }

    public function skellman_option_section_info() {

    }

    public function about_text_0_callback() {
        printf(
            '<textarea class="large-text" rows="5" name="skellman_option_option_name[about_text_0]" id="about_text_0">%s</textarea>',
            isset( $this->skellman_option_options['about_text_0'] ) ? esc_attr( $this->skellman_option_options['about_text_0']) : ''
        );
    }

    public function about_job_title_1_callback() {
        printf(
            '<input class="regular-text" type="text" name="skellman_option_option_name[about_job_title_1]" id="about_job_title_1" value="%s">',
            isset( $this->skellman_option_options['about_job_title_1'] ) ? esc_attr( $this->skellman_option_options['about_job_title_1']) : ''
        );
    }

    public function linkedin_2_callback() {
        printf(
            '<input class="regular-text" type="url" name="skellman_option_option_name[linkedin_2]" id="linkedin_2" value="%s">',
            isset( $this->skellman_option_options['linkedin_2'] ) ? esc_attr( $this->skellman_option_options['linkedin_2']) : ''
        );
    }

    public function github_3_callback() {
        printf(
            '<input class="regular-text" type="url" name="skellman_option_option_name[github_3]" id="github_3" value="%s">',
            isset( $this->skellman_option_options['github_3'] ) ? esc_attr( $this->skellman_option_options['github_3']) : ''
        );
    }

    public function stackoverflow_4_callback() {
        printf(
            '<input class="regular-text" type="url" name="skellman_option_option_name[stackoverflow_4]" id="stackoverflow_4" value="%s">',
            isset( $this->skellman_option_options['stackoverflow_4'] ) ? esc_attr( $this->skellman_option_options['stackoverflow_4']) : ''
        );
    }

    public function px_5_callback() {
		printf(
			'<input class="regular-text" type="url" name="skellman_option_option_name[px_5]" id="px_5" value="%s">',
			isset( $this->skellman_option_options['px_5'] ) ? esc_attr( $this->skellman_option_options['px_5']) : ''
		);
	}

}

/*
 * Retrieve this value with:
 * $skellman_option_options = get_option( 'skellman_option_option_name' ); // Array of All Options
 * $about_text_0 = $skellman_option_options['about_text_0']; // About Text
 * $about_job_title_1 = $skellman_option_options['about_job_title_1']; // About Job Title
 * $linkedin_2 = $skellman_option_options['linkedin_2']; // LinkedIn
 * $github_3 = $skellman_option_options['github_3']; // Github
 * $stackoverflow_4 = $skellman_option_options['stackoverflow_4']; // Stackoverflow
 * $px_5 = $skellman_option_options['px_5']; // 500px
 */
