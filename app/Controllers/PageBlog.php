<?php


namespace App\Controllers;


use Sober\Controller\Controller;
use WP_Query;

class PageBlog extends Controller
{
    public function posts()
    {
        $args = array(
            'posts_per_page'   => -1,
            'post_type'        => 'post',
        );

        return new WP_Query( $args );
    }
}
