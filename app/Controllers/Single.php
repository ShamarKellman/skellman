<?php


namespace App\Controllers;


use Sober\Controller\Controller;

class Single extends Controller {

//$url = urlencode(get_the_permalink());
//$title = urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));
//$media = urlencode(get_the_post_thumbnail_url(get_the_ID(), 'full'));

    public function shareTitle() {
        return urlencode(html_entity_decode(get_the_title($this->post->ID), ENT_COMPAT, 'UTF-8'));
    }

    public function shareUrl() {
        return urlencode(get_the_permalink($this->post->ID));
    }

    public function shareImage() {
        return urlencode(get_the_post_thumbnail_url($this->post->id, 'full'));
    }

    public function whatsappText() {
        return urlencode(html_entity_decode(get_the_title($this->post->ID), ENT_COMPAT, 'UTF-8') . ' - ' . get_the_permalink($this->post->ID));
    }
}
