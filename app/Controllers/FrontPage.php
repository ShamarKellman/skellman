<?php

namespace App\Controllers;

use Sober\Controller\Controller;
/*
 * Retrieve this value with:
 * $skellman_option_options = get_option( 'skellman_option_option_name' ); // Array of All Options
 * $about_text_0 = $skellman_option_options['about_text_0']; // About Text
 * $about_job_title_1 = $skellman_option_options['about_job_title_1']; // About Job Title
 * $linkedin_2 = $skellman_option_options['linkedin_2']; // LinkedIn
 * $github_3 = $skellman_option_options['github_3']; // Github
 * $stackoverflow_4 = $skellman_option_options['stackoverflow_4']; // Stackoverflow
 * $px_5 = $skellman_option_options['px_5']; // 500px
 */

class FrontPage extends Controller
{

    public function projects()
    {
        $args = [
            'post_type'           => 'project',
            'posts_per_page'      => 3,
            'orderby'             => 'date',
            'order'               => 'DESC',
        ];

        return $query = new \WP_Query($args);
    }

    public function aboutText()
    {
        return get_option( 'skellman_option_option_name' )['about_text_0'];
    }

    public function linkedin()
    {
        return get_option( 'skellman_option_option_name' )['linkedin_2'];
    }

    public function github()
    {
        return get_option( 'skellman_option_option_name' )['github_3'];
    }

    public function stackoverflow()
    {
        return get_option( 'skellman_option_option_name' )['stackoverflow_4'];
    }

    public function px()
    {
        return get_option( 'skellman_option_option_name' )['px_5'];
    }

    public function jobTitle()
    {
        return get_option( 'skellman_option_option_name' )['about_job_title_1'];
    }
}
