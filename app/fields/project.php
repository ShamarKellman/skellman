<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$projectFields = new FieldsBuilder('Info');
$projectFields
    ->addSelect('project_status', [
        'required' => true,
        'allow_null' => false,
        'placeholder' => 'Add A Project Status',
    ])->addChoices([
        'planning' => 'In Planning',
        'progress' => 'In Progress',
        'completed' => 'Completed',
    ])
    ->addText('project_url', [
        'placeholder' => 'Site URL',
        'type' => 'url'
    ])
    ->addSelect('project_tech', [
        'required' => false,
        'placeholder' => 'Add Technologies Used',
        'multiple' => true,
        'ui' => true,
    ])
    ->addChoices([
        'php' => 'PHP',
        'vue' => 'VueJs',
        'tailwind' => 'Tailwind CSS',
        'laravel' => 'Laravel',
        'android' => 'Android',
        'ios' => 'iOS',
        'wordpress' => 'Wordpress'
    ])
    ->setLocation('post_type', '==', 'project');

return $projectFields;
