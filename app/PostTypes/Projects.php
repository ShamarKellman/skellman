<?php

namespace App\PostTypes;

use PostTypes\PostType;

class Projects {
    public $type;

    public function __construct()
    {
        $this->type = new PostType('project');
    }

    public function setup() {
        $this->type->icon('dashicons-awards');
        $this->type->options([
            'supports' => [
                'title', 'editor', 'custom-fields', 'thumbnail'
            ]
        ]);
        return $this;
    }

    public function register()
    {
        $this->type->register();
    }
}
